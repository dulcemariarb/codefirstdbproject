﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
 using System.Linq;
 using System.Text;

 namespace CodeFirstDBProject
 {
         public class ProductContext : DbContext
        {
            public ProductContext() : base(nameOrConnectionString:
                "myConnection") {  }
            public DbSet<Category> Categories { get; set; }
            public DbSet<Product> Products { get; set; }

            protected override void OnModelCreating(DbModelBuilder
            modelBuilder)
            {
                    modelBuilder.HasDefaultSchema("public") ;
                    base.OnModelCreating(modelBuilder);
            }
        }
 }